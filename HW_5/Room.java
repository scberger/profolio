//Room class for object HW 5
//Author: Shayna Bergeron
//Date: 7/11/2018

public class Room{
	private String description;
	private Room north;
	private Room east;
	private Room west;
	private Room south;

	/*constructor*/
	public Room(String description){
		this.description = description;
		this.north = null;
		this.south = null;
		this.east = null;
		this.west = null;
	}//end of constructor
	
	/*getters and setters*/
	/*setters for n,s,e,w*/
	public void setNorth(Room north){
		this.north = north;
	}
	public void setSouth(Room south){
		this.south = south;
	}
	public void setEast(Room east){
		this.east = east;
	}
	public void setWest(Room west){
		this.west = west;
	}
	/*getters for n,s,e,w*/
	public Room getNorth(){
		return this.north;
	}
	public Room getSouth(){
		return this.south;
	}
	public Room getEast(){
		return this.east;
	}
	public Room getWest(){
		return this.west;
	}
	
	/*setters for exits*/
	public void setExits(Room n, Room s, Room e, Room w){
		this.north=n;
		this.south=s;
		this.east=e;
		this.west=w;
	}//exits
	
	public String getDescription(){
		return this.description;
	}

	public String getExits(){
		String result = "";
		
		if(this.south != null){
			String temp1 = result+"South: "+this.south.description+"\t";
			result = temp1;
		}
		if(this.north != null){
			String temp1 = result+"North: "+this.north.description+"\t";
			result = temp1;
		}
		if(this.east != null){
			String temp1 = result+"East: "+this.east.description+"\t";
			result = temp1;
		}
		if(this.west != null){
			String temp1 = result+"West: "+this.west.description+"\t";
			result = temp1;
		}
		return result;
	}//getExits()

	public String toString(){
		return this.description+"\n"+this.getExits();
	}//to string()
}// end of class