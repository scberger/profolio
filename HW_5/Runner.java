//Runner for HW 5
//Author: Shayna Bergeron
//Date: 7/11/2018

import java.util.Scanner;

public class Runner{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		Room balcony = new Room("Balcony");
		Room bedroom1 = new Room("Bedroom1");
		Room bedroom2 = new Room("Bedroom2");
		Room dining = new Room("Dining");
		Room kitchen = new Room("Kitchen");
		Room northHall = new Room("North Hall");
		Room southHall = new Room("South Hall");

		Dungeon d = new Dungeon(balcony,bedroom1,bedroom2,dining,kitchen,northHall,southHall);
		Room curRoom = d.getRoom0();

		Room nextRoom = null;
		boolean gameOver = false;
		char user;

		while(!gameOver) {

		System.out.println("Current Room:"+curRoom.toString());

		System.out.print("Enter your choice(E or W or N or S or Q):");
		user = input.next().charAt(0);

		switch(user) {
		case 'E':
		nextRoom = curRoom.getEast();
		break;
		case 'W':
		nextRoom = curRoom.getWest();
		break;
		case 'N':
		nextRoom = curRoom.getNorth();
		break;
		case 'S':
		nextRoom = curRoom.getSouth();
		break;
		case 'Q':
		gameOver=true;
		break;

		default :
		System.out.println("Invalid choice");
		}

		if(nextRoom==null) {
		System.out.println("Invalid Exit");
		}
		else
		curRoom = nextRoom;
		}

		System.out.println("\nThanks for playing");
		}

}//end of runner class