// Iteration 5/7 - Produces new calculations for the monster and player stats

//Author: Shayna Bergeron
//Date: 6/13/2018

import java.util.Scanner;

public class CombatCalc5{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		/* Monster data variables */
		String goblin;
		goblin = "Goblin"; //Initializing the monster's name
		int monsterHealth;
		monsterHealth = 100; //Initializing the monster's health
		int monsterAttackPower;
		monsterAttackPower = 15; //Initializing the monster's attack power

		/*Hero data variables*/
		int heroHealth;
		heroHealth = 100; //Initializing the hero's health
		int heroAttackPower;
		heroAttackPower = 12; //Initializing the hero's attack power
		int heroMagicPower;
		heroMagicPower = 0; //Initializing the hero's magic power

		/*Prints out stats of player and monster*/
		System.out.println("You are fighting a " +goblin+ "!"); 
		System.out.println("The monster's HP: " +monsterHealth);
		System.out.println("Your HP: " +heroHealth);
		System.out.println("Your MP: " +heroMagicPower);

		/*Sets up for user input for combat options*/
		System.out.println("\nCombat Options: ");
		System.out.println("1.) Sword Attack"); 
		System.out.println("2.) Cast Spell");
		System.out.println("3.) Charge Mana");
		System.out.println("4.) Run Away");
		System.out.print("What action do you want to perform? ");
		int userOption;
		userOption = input.nextInt();

		/*Responds to player's input*/
		if (userOption == 1){
			monsterHealth = monsterHealth - heroAttackPower; //Calculating the new monster health
			System.out.println("\nYou strike the " +goblin+ "with your sword for " +heroAttackPower+ " damage.");
		}
		else if(userOption == 2){
			monsterHealth = monsterHealth / 2; //Calculating new monster health
			System.out.println("\nYou cast the weaken spell on the monster.");
		}
		else if(userOption == 3){
			heroMagicPower++; //Calculating new magic power
			System.out.println("\nYou focus and charge your magic power.");
		}
		else if(userOption == 4){
			System.out.println("\nYou run away!");
		}
		else{
			System.out.println("\nI don't understand this command.");
		}
	}
}