// Iteration 7/7 - Verifies player's stats to make sure it meets certain criteria
//Author: Shayna Bergeron
//Date: 6/13/2018

import java.util.Scanner;

public class CombatCalc7{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		/* Monster data variables */
		String goblin;
		goblin = "Goblin"; //Initializing the monster's name
		int monsterHealth;
		monsterHealth = 100; //Initializing the monster's health
		int monsterAttackPower;
		monsterAttackPower = 15; //Initializing the monster's attack power

		/*Hero data variables*/
		int heroHealth;
		heroHealth = 100; //Initializing the hero's health
		int heroAttackPower;
		heroAttackPower = 12; //Initializing the hero's attack power
		int heroMagicPower;
		heroMagicPower = 0; //Initializing the hero's magic power
		
		boolean loopCombat = true; //Delcare loop and initialize it
		/*Start of while loop, while the variable is true*/	
		while (loopCombat == true){
			/*Prints out stats of player and monster*/
			System.out.println("You are fighting a " +goblin+ "!"); 
			System.out.println("The monster's HP: " +monsterHealth);
			System.out.println("Your HP: " +heroHealth);
			System.out.println("Your MP: " +heroMagicPower);

			/*Sets up for user input for combat options*/
			System.out.println("\nCombat Options: ");
			System.out.println("1.) Sword Attack"); 
			System.out.println("2.) Cast Spell");
			System.out.println("3.) Charge Mana");
			System.out.println("4.) Run Away");
			System.out.print("What action do you want to perform? ");
			int userOption;
			userOption = input.nextInt();

			/*Responds to player's input*/
			if (userOption == 1){
				monsterHealth = monsterHealth - heroAttackPower; //Calculating the new monster health
				System.out.println("\nYou strike the " +goblin+ "with your sword for " +heroAttackPower+ " damage.\n");
			}
			else if(userOption == 2){
				if (heroMagicPower >= 3){
					heroMagicPower = heroMagicPower - 3;
				} //Reduces player's mana points
				monsterHealth = monsterHealth / 2; //Calculating new monster health
				System.out.println("\nYou cast the weaken spell on the monster.\n");
			}
			else if(userOption == 3){
				heroMagicPower++; //Calculating new magic power
				System.out.println("\nYou focus and charge your magic power.\n");
			}
			else if(userOption == 4){
				loopCombat = false; //Stops loop
				System.out.println("\nYou run away!\n");
			}
			else{
				System.out.println("\nI don't understand this command.\n");
			
			}
			/*Stops loops once monster reaches 0 or below health*/
			if (monsterHealth <= 0){
				loopCombat = false; 
				System.out.println("Congrats! You have defeated the " +goblin+ "!");
			}
		}
	}
}