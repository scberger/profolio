//Making a fraction class
//Author: Shayna Bergeron
//Date: 7/2/2018

public class Fraction{
	private int numerator;
	private int denominator;

	//constructor
	public Fraction(int numerator, int denominator){
		this.numerator = numerator;
		this.denominator = denominator;
	}// end of constructor 

	public Fraction add(Fraction otherFraction){
		int commonDenom = this.denominator * otherFraction.denominator;
		int num1 = this.numerator * otherFraction.denominator;
		int num2 = otherFraction.numerator * this.denominator;
		int newNum = num1 + num2;
		return new Fraction(newNum, commonDenom);
	}// end of add

	@Override
	public String toString(){
		return this.numerator + "/" + this.denominator;
	}// end of toString

	public Fraction subtract(Fraction otherFraction){
		int commonDenom = this.denominator * otherFraction.denominator;
		int num1 = this.numerator * otherFraction.denominator;
		int num2 = otherFraction.numerator * this.denominator;
		int newNum = num1 - num2;
		return new Fraction(newNum, commonDenom);
	}// end of subtraction

	public Fraction multi(Fraction otherFraction){
		int num1 = this.denominator * otherFraction.denominator;
		int num2 = this.numerator * otherFraction.numerator;
		return new Fraction(num1, num2);
	}// end of multiplication

	public Fraction division(Fraction otherFraction){
		int num1 = this.denominator * otherFraction.numerator;
		int num2 = this.numerator * otherFraction.denominator;
		return new Fraction(num1, num2);
	}// end of division

}// end of class