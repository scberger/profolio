//array of 10 elements and months
//Author: Shayna Bergeron
//Date: 6/27/2018

import java.util.Random;

public class ExerciseOne{
	public static void main(String[] args){
		Random ranIntGenerator = new Random();
		//first part of the exercise
		int[] myInts = new int[10];
		//populates array
		for(int i = 0; i < myInts.length; i++){
			myInts[i] = ranIntGenerator.nextInt();
		}//end of for loop

		for(int i=0; i < myInts.length; i++){
			System.out.println(myInts[i]);
		}//prints values in array

		//part 2 of exercise
		String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
							 "Aug", "Sep", "Oct", "Nov", "Dec"};
		
		System.out.println(months[ranIntGenerator.nextInt(months.length)]);
		
	}//end of main
}//end of class