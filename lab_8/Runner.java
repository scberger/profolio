public class Runner{
	public static void main(String[] args) {
		Point2D point1 = new Point2D(1.5, 2.5);
		Point2D point2 = new Point2D(4.0, 2.5);
		Point2D point3 = new Point2D(1.5, 2.5);
		Point2D point5 = new Point2D(3.5, 5.0);

		Object point4 = (Object)point1;

		SetOfPoints set1 = new SetOfPoints(point1, point2, point3);
		LineSegment line1 = new LineSegment(point1, point2);
		Triangle tri = new Triangle(point1, point2, point3);
		LineSegment line2 = new LineSegment();
		line2.add(point1);

		SetOfPoints set2 = new Triangle();
		set2.add(point1);
		System.out.println("set2.toString(): "+set2.toString());
		set2.add(point2);
		set2.add(point5);
		set2 = (Triangle)set2;
		Triangle tri3 = (Triangle)set2;
		System.out.println("set2 area: "+tri3.area());

		SetOfPoints set3 = new SetOfPoints(point1, point2);
		LineSegment line3 = (LineSegment)set3;
		System.out.println("length of line3: "+line3.length());


		System.out.println("point1 equals point2: "+point1.equals(point2));
		System.out.println("point1 equals point3: "+point1.equals(point3));
		System.out.println("point1 equals point1: "+point1.equals(point1));


		System.out.println("point1 toString: "+point1.toString());
		System.out.println("point2 toString: "+point2.toString());
		System.out.println("point3 toString: "+point3.toString());
		System.out.println("point4 toString: "+point4.toString());

	}
}