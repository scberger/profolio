//This is a looper program that loops a specified number of times

//Author: Shayna Bergeron
//Version: 6/13/2018

import java.util.Scanner;

public class Looper{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int iterations;
		System.out.print("How many loops would you like: ");
		iterations = input.nextInt();

		for(int i=1; i <= iterations; i++){
			System.out.println("This is loop number " + i + " of " + iterations);

		}
		System.out.println("\nDone looping!!");
	}
}